from django.urls import path
from admin_panel.views import test_view

urlpatterns = [
    path('test/', test_view, name='test'),
]