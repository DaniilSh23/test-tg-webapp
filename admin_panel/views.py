from django.shortcuts import render


def test_view(request):
    if request.method == 'GET':
        context = {'test_data': 'ITS TEST DATA!'}
        return render(request, 'forms.html', context=context)
        # return render(request, 'index.html', context=context)
